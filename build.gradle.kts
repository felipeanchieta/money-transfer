import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm") version "1.3.31"
    jacoco
}

group = "io.github.felipeanchieta"
version = "1.0-SNAPSHOT"

repositories {
    jcenter()
}

application {
    mainClassName = "io.github.felipeanchieta.moneytransfer.ApplicationKt"
}

val ktorVersion = "1.2.1"
val koinVersion = "2.0.1"

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-jackson:$ktorVersion")
    implementation("org.koin:koin-logger-slf4j:$koinVersion")
    implementation("org.koin:koin-ktor:$koinVersion")

    testImplementation("org.junit.jupiter:junit-jupiter:5.4.2")
    testImplementation("io.mockk:mockk:1.9.3")
    testImplementation("io.ktor:ktor-server-test-host:$ktorVersion") {
        exclude("junit")
        exclude("org.mockito")
    }
    testImplementation("org.koin:koin-test:$koinVersion") {
        exclude("junit")
        exclude("org.mockito")
    }
}

jacoco {
    toolVersion = "0.8.4"
    reportsDir = file("$buildDir/reports/jacoco")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.allWarningsAsErrors = true
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<Test> {
    useJUnitPlatform()
}
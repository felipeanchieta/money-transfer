# money-transfer

This application comprehends an interpretation for the Revolut back-end technical challenge.

Here we have a RESTful application written in Kotlin with Koin + Ktor for lightweight dependency
injection and web framework. A ConcurrencyHashMap has been used for proving the whole concept
without depending on a database or in-memory databases such as H2, for the sake of brevity.

##  Running

The application has a Gradle Wrapper that can be run with:

```bash
./gradlew clean build run
```

## Testing

JUnit 5 tests can be run with

```bash
./gradlew test
```

if you want coverage report as well, run:

```bash
./gradlew test jacocoTestReport
```

## Endpoints for evaluation

### Health endpoint:
```bash
curl --request GET \
  --url http://localhost:8080/health
```

The expected output is HTTP 200.

### Create an account for testing purposes:
```bash
curl --request POST \
  --url http://localhost:8080/accounts \
  --header 'content-type: application/json' \
  --data '{
	"initialDeposit": 1800.0
}'
```

The expected output is HTTP 201.

### Retrieving account's balance, for testing purposes:

Replace :uuid with a valid UUID, pre-existent into the application

```bash
curl --request GET \
  --url http://localhost:8080/accounts/:uuid/balance
```

The expected output is HTTP 200, and the balance itself.

### Transferring money

Replace both :senderUUID and :recipientUUID with valid UUIDs, pre-existent into the application

```bash
curl --request POST \
  --url http://localhost:8080/accounts/:senderUUID/transfer \
  --header 'content-type: application/json' \
  --data '{
	"recipientId": ":recipientUUID",
	"amount": 20000.0
}'
```

The expected output is HTTP 202, and the amount transferred itself.

### Error Handling

The error handling is self-explanatory in the integration tests. The consumer
of this application should only handle HTTP codes as for now.
package io.github.felipeanchieta.moneytransfer.persistence

import io.github.felipeanchieta.moneytransfer.account.commons.Account
import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap

internal class LocalAccountsGatewayImplTest {
    @Test
    fun `should return account when existent`() {
        val account =
            Account(id = UUID.randomUUID(), balance = 123.4)
        val stubMap = ConcurrentHashMap<UUID, Account>(mapOf(account.id to account))
        val gateway: AccountsGateway = LocalAccountsGatewayImpl(stubMap)

        assertEquals(account, gateway.findById(account.id))
    }

    @Test
    fun `should return null when account is non-existent`() {
        val gateway: AccountsGateway = LocalAccountsGatewayImpl()
        assertNull(gateway.findById(id = UUID.randomUUID()))
    }

    @Test
    fun `should persist an account`() {
        val account =
            Account(id = UUID.randomUUID(), balance = 123.4)
        val stubMap = ConcurrentHashMap<UUID, Account>()
        val gateway: AccountsGateway = LocalAccountsGatewayImpl(stubMap)

        assertEquals(account, gateway.save(account))
        assertEquals(mapOf(account.id to account), stubMap)
    }

    @Test
    fun `should persist multiples account at the same time`() {
        val accounts = listOf(
            Account(id = UUID.randomUUID(), balance = 123.4),
            Account(id = UUID.randomUUID(), balance = 123.4)
        )
        val stubMap = ConcurrentHashMap<UUID, Account>()
        val gateway: AccountsGateway = LocalAccountsGatewayImpl(stubMap)

        assertEquals(accounts, gateway.saveAll(accounts))
        assertEquals(accounts.map { account -> account.id to account }.toMap(), stubMap)
    }
}
package io.github.felipeanchieta.moneytransfer.account.transfer

import io.github.felipeanchieta.moneytransfer.account.commons.Account
import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import io.github.felipeanchieta.moneytransfer.account.commons.exceptions.AccountNotFoundException
import io.github.felipeanchieta.moneytransfer.account.transfer.core.MoneyTransferInteractor
import io.github.felipeanchieta.moneytransfer.account.transfer.core.exceptions.InsufficientBalanceException
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifySequence
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.UUID

internal class MoneyTransferInteractorTest {
    private val gateway = mockk<AccountsGateway>()
    private val interactor =
        MoneyTransferInteractor(gateway)

    @Test
    fun `should transfer money when sender has enough balance`() {
        val amount = 2000.0
        val sender =
            Account(id = UUID.randomUUID(), balance = 3000.0)
        val recipient =
            Account(id = UUID.randomUUID(), balance = 500.0)

        every {
            gateway.findById(sender.id)
        } returns sender

        every {
            gateway.findById(recipient.id)
        } returns recipient

        every {
            gateway.saveAll(any())
        } returnsArgument 0

        // Call
        interactor.transferMoney(amount, sender.id, recipient.id)

        verifySequence {
            gateway.findById(sender.id)
            gateway.findById(recipient.id)
            gateway.saveAll(
                listOf(
                    Account(
                        id = sender.id,
                        balance = sender.balance - amount
                    ),
                    Account(
                        id = recipient.id,
                        balance = recipient.balance + amount
                    )
                )
            )
        }
    }

    @Test
    fun `should not transfer money when sender has not enough balance`() {
        val amount = 2000.0
        val sender =
            Account(id = UUID.randomUUID(), balance = 1999.9)
        val recipientId = UUID.randomUUID()

        every {
            gateway.findById(sender.id)
        } returns sender

        // Call
        val ex = assertThrows<InsufficientBalanceException> {
            interactor.transferMoney(amount, sender.id, recipientId)
        }

        assertEquals("account ${sender.id} does not have enough balance for this operation.", ex.message)
        verify(exactly = 1) {
            gateway.findById(sender.id)
        }
    }


    @Test
    fun `should not transfer money when sender cannot be found`() {
        val amount = 2000.0
        val senderId = UUID.randomUUID()
        val recipientId = UUID.randomUUID()

        every {
            gateway.findById(senderId)
        } returns null

        // Call
        val ex = assertThrows<AccountNotFoundException> {
            interactor.transferMoney(amount, senderId, recipientId)
        }

        assertEquals("account $senderId cannot be found.", ex.message)
        verify(exactly = 1) {
            gateway.findById(senderId)
        }
    }

    @Test
    fun `should not transfer money when recipient cannot be found`() {
        val amount = 2000.0
        val sender =
            Account(id = UUID.randomUUID(), balance = 2000.0)
        val recipientId = UUID.randomUUID()

        every {
            gateway.findById(sender.id)
        } returns sender

        every {
            gateway.findById(recipientId)
        } returns null

        // Call
        val ex = assertThrows<AccountNotFoundException> {
            interactor.transferMoney(amount, sender.id, recipientId)
        }

        assertEquals("account $recipientId cannot be found.", ex.message)
        verifySequence {
            gateway.findById(sender.id)
            gateway.findById(recipientId)
        }
    }
}
package io.github.felipeanchieta.moneytransfer.account.create

import io.github.felipeanchieta.moneytransfer.account.commons.Account
import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import io.github.felipeanchieta.moneytransfer.account.create.core.AccountCreationInteractor
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.UUID

internal class AccountCreationInteractorTest {
    private val gateway = mockk<AccountsGateway>()
    private val interactor =
        AccountCreationInteractor(gateway)

    @Test
    fun `should create an account`() {
        val initialDeposit = 90.0

        every {
            gateway.save(any())
        } returnsArgument 0

        val stubUUID = { UUID.fromString("5ee59c08-7939-430b-b3e5-6912c00413b6") }
        val account = interactor.createAccount(initialDeposit, stubUUID)
        assertEquals(
            Account(
                id = stubUUID(),
                balance = initialDeposit
            ), account
        )

        verify(exactly = 1) {
            gateway.save(any())
        }
    }
}
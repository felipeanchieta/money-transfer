package io.github.felipeanchieta.moneytransfer.account.balance

import io.github.felipeanchieta.moneytransfer.account.balance.core.BalanceQueryInteractor
import io.github.felipeanchieta.moneytransfer.account.commons.Account
import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import io.github.felipeanchieta.moneytransfer.account.commons.exceptions.AccountNotFoundException
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.UUID

internal class BalanceQueryInteractorTest {
    private val gateway = mockk<AccountsGateway>()
    private val interactor =
        BalanceQueryInteractor(gateway)

    @Test
    fun `should retrieve balance for an existent account`() {
        val accountId = UUID.randomUUID()
        val balance = 3000.0

        every {
            gateway.findById(accountId)
        } returns Account(
            id = accountId,
            balance = balance
        )

        assertEquals(balance, interactor.getBalanceForAccount(accountId))

        verify(exactly = 1) {
            gateway.findById(accountId)
        }
    }

    @Test
    fun `should throw for a non-existent account`() {
        val accountId = UUID.randomUUID()
        val balance = 3000.0

        every {
            gateway.findById(accountId)
        } returns null

        val ex = assertThrows<AccountNotFoundException> {
            assertEquals(balance, interactor.getBalanceForAccount(accountId))
        }

        assertEquals("account $accountId cannot be found.", ex.message)
        verify(exactly = 1) {
            gateway.findById(accountId)
        }
    }
}
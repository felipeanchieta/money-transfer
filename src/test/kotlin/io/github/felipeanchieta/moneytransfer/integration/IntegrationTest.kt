package io.github.felipeanchieta.moneytransfer.integration

import io.github.felipeanchieta.moneytransfer.account.create.core.AccountCreationInteractor
import io.github.felipeanchieta.moneytransfer.misc.module
import io.ktor.application.Application
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.contentType
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.koin.test.AutoCloseKoinTest
import org.koin.test.inject
import java.util.UUID

class IntegrationTest : AutoCloseKoinTest() {
    private val creationInteractor: AccountCreationInteractor by inject()

    @Test
    fun `should have a health indicator endpoint that returns HTTP 200`() = withTestApplication(Application::module) {
        with(handleRequest(HttpMethod.Get, "/health")) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals("Healthy :)", response.content)
        }
    }

    @Test
    fun `should create a new account and return HTTP 201`() = withTestApplication(Application::module) {
        with(handleRequest(HttpMethod.Post, "/accounts") {
            addHeader("Content-Type", "application/json")
            setBody("""{"initialDeposit":900}""")
        }) {
            assertEquals(HttpStatusCode.Created, response.status())
            assertTrue(response.contentType().match(ContentType.Application.Json))
        }
    }

    @Test
    fun `should get an account balance and return HTTP 200`() = withTestApplication(Application::module) {
        // Create before all
        val accountId = "415fe383-40d0-42ba-a022-4ea80b93aba1"
        creationInteractor.createAccount(200.0) {
            UUID.fromString(accountId)
        }

        with(handleRequest(HttpMethod.Get, "/accounts/$accountId/balance")) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertTrue(response.contentType().match(ContentType.Application.Json))
        }
    }

    @Test
    fun `should return HTTP 404 when cannot find account`() = withTestApplication(Application::module) {
        val accountId = "415fe383-40d0-42ba-a022-4ea80b93aba1"

        with(handleRequest(HttpMethod.Get, "/accounts/$accountId/balance")) {
            assertEquals(HttpStatusCode.NotFound, response.status())
        }
    }

    @Test
    fun `should transfer accounts money and return HTTP 202`() = withTestApplication(Application::module) {
        // Create accounts before
        val senderId = "415fe383-40d0-42ba-a022-4ea80b93aba1"
        val recipientId = "717b8902-e2df-4409-9c87-d730503e2ed2"
        creationInteractor.createAccount(200.0) {
            UUID.fromString(senderId)
        }
        creationInteractor.createAccount(200.0) {
            UUID.fromString(recipientId)
        }

        with(handleRequest(HttpMethod.Post, "/accounts/$senderId/transfer") {
            addHeader("Content-Type", "application/json")
            setBody("""{"recipientId":"$recipientId","amount":50.0}""")
        }) {
            assertEquals(HttpStatusCode.Accepted, response.status())
            assertTrue(response.contentType().match(ContentType.Application.Json))
        }
    }

    @Test
    fun `should return HTTP 422 when trying to transfer without enough balance`() =
        withTestApplication(Application::module) {
            // Create accounts before
            val senderId = "415fe383-40d0-42ba-a022-4ea80b93aba1"
            val recipientId = "717b8902-e2df-4409-9c87-d730503e2ed2"
            creationInteractor.createAccount(200.0) {
                UUID.fromString(senderId)
            }
            creationInteractor.createAccount(200.0) {
                UUID.fromString(recipientId)
            }

            with(handleRequest(HttpMethod.Post, "/accounts/$senderId/transfer") {
                addHeader("Content-Type", "application/json")
                setBody("""{"recipientId":"$recipientId","amount":201.0}""")
            }) {
                assertEquals(HttpStatusCode.UnprocessableEntity, response.status())
            }
        }

    @Test
    fun `should return HTTP 400 for malformed JSONs`() = withTestApplication(Application::module) {
        with(handleRequest(HttpMethod.Post, "/accounts") {
            addHeader("Content-Type", "application/json")
            setBody("""{"balanced":900}""")
        }) {
            assertEquals(HttpStatusCode.BadRequest, response.status())
        }
    }

    @Test
    fun `should return HTTP 400 for empty JSONs`() = withTestApplication(Application::module) {
        with(handleRequest(HttpMethod.Post, "/accounts") {
            addHeader("Content-Type", "application/json")
            setBody("{}")
        }) {
            assertEquals(HttpStatusCode.BadRequest, response.status())
        }
    }

    @Test
    fun `should return HTTP 400 for empty bodies`() = withTestApplication(Application::module) {
        with(handleRequest(HttpMethod.Post, "/accounts") {
            addHeader("Content-Type", "application/json")
            setBody("")
        }) {
            assertEquals(HttpStatusCode.BadRequest, response.status())
        }
    }
}
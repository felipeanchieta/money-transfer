package io.github.felipeanchieta.moneytransfer.misc.config

import io.github.felipeanchieta.moneytransfer.account.balance.core.BalanceQueryInteractor
import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import io.github.felipeanchieta.moneytransfer.account.create.core.AccountCreationInteractor
import io.github.felipeanchieta.moneytransfer.account.transfer.core.MoneyTransferInteractor
import io.github.felipeanchieta.moneytransfer.persistence.LocalAccountsGatewayImpl
import org.koin.Logger.slf4jLogger
import org.koin.core.KoinApplication
import org.koin.dsl.module

val applicationModules = module {
    single { AccountCreationInteractor(get()) }
    single { BalanceQueryInteractor(get()) }
    single { MoneyTransferInteractor(get()) }
    single<AccountsGateway> { LocalAccountsGatewayImpl() }
}

fun KoinApplication.koinConfig() {
    slf4jLogger()
    modules(applicationModules)
}
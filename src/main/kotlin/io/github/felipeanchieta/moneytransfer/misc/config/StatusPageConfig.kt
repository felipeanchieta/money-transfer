package io.github.felipeanchieta.moneytransfer.misc.config

import com.fasterxml.jackson.core.JsonProcessingException
import io.github.felipeanchieta.moneytransfer.account.commons.exceptions.AccountNotFoundException
import io.github.felipeanchieta.moneytransfer.account.transfer.core.exceptions.InsufficientBalanceException
import io.ktor.application.call
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger("StatusPage")

fun StatusPages.Configuration.statusPageConfig() {
    exception<JsonProcessingException> { cause ->
        call.respond(HttpStatusCode.BadRequest)
        logger.error(cause.message)
    }
    exception<InsufficientBalanceException> { cause ->
        call.respond(HttpStatusCode.UnprocessableEntity)
        logger.error(cause.message)
    }
    exception<AccountNotFoundException> { cause ->
        call.respond(HttpStatusCode.NotFound)
        logger.error(cause.message)
    }
}
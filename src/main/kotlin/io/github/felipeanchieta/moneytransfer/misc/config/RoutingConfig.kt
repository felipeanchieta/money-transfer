package io.github.felipeanchieta.moneytransfer.misc.config

import io.github.felipeanchieta.moneytransfer.account.balance.presenter.getAccountBalanceEndpoint
import io.github.felipeanchieta.moneytransfer.account.create.presenter.createAccountEndpoint
import io.github.felipeanchieta.moneytransfer.account.transfer.presenter.transferMoneyEndpoint
import io.github.felipeanchieta.moneytransfer.health.healthEndpoint
import io.ktor.routing.Routing

fun Routing.root() {
    healthEndpoint()
    createAccountEndpoint()
    getAccountBalanceEndpoint()
    transferMoneyEndpoint()
}
package io.github.felipeanchieta.moneytransfer.misc.config

import com.fasterxml.jackson.databind.DeserializationFeature
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson

fun ContentNegotiation.Configuration.jacksonConfig() {
    jackson {
        enable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES)
    }
}
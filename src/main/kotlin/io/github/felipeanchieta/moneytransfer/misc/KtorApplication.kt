package io.github.felipeanchieta.moneytransfer.misc

import io.github.felipeanchieta.moneytransfer.misc.config.jacksonConfig
import io.github.felipeanchieta.moneytransfer.misc.config.koinConfig
import io.github.felipeanchieta.moneytransfer.misc.config.root
import io.github.felipeanchieta.moneytransfer.misc.config.statusPageConfig
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.routing.Routing
import org.koin.ktor.ext.Koin

fun Application.module() {
    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation) {
        jacksonConfig()
    }
    install(Routing) {
        root()
    }
    install(StatusPages) {
        statusPageConfig()
    }
    install(Koin) {
        koinConfig()
    }
}
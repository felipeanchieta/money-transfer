package io.github.felipeanchieta.moneytransfer.account.commons

import java.util.UUID

interface AccountsGateway {
    fun findById(id: UUID): Account?
    fun save(account: Account): Account
    fun saveAll(accounts: List<Account>): List<Account>
}
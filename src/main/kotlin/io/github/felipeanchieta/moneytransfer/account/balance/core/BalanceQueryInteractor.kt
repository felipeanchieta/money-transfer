package io.github.felipeanchieta.moneytransfer.account.balance.core

import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import io.github.felipeanchieta.moneytransfer.account.commons.exceptions.AccountNotFoundException
import java.util.UUID

class BalanceQueryInteractor(private val gateway: AccountsGateway) {
    fun getBalanceForAccount(accountId: UUID): Double {
        return gateway.findById(id = accountId)?.balance
            ?: throw AccountNotFoundException(accountId)
    }
}
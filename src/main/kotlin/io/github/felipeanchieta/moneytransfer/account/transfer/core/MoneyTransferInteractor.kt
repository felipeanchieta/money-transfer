package io.github.felipeanchieta.moneytransfer.account.transfer.core

import io.github.felipeanchieta.moneytransfer.account.commons.Account
import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import io.github.felipeanchieta.moneytransfer.account.commons.exceptions.AccountNotFoundException
import io.github.felipeanchieta.moneytransfer.account.transfer.core.exceptions.InsufficientBalanceException
import java.util.UUID

class MoneyTransferInteractor(private val gateway: AccountsGateway) {
    fun transferMoney(amount: Double, senderId: UUID, recipientId: UUID) {
        val sender = gateway.findById(senderId)
            ?: throw AccountNotFoundException(senderId)

        if (sender.balance < amount) {
            throw InsufficientBalanceException(sender.id)
        }

        val recipient = gateway.findById(recipientId)
            ?: throw AccountNotFoundException(recipientId)

        gateway.saveAll(
            listOf(
                Account(
                    id = senderId,
                    balance = sender.balance - amount
                ),
                Account(
                    id = recipientId,
                    balance = recipient.balance + amount
                )
            )
        )
    }
}
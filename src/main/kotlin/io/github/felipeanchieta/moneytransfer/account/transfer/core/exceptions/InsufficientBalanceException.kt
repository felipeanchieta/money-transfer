package io.github.felipeanchieta.moneytransfer.account.transfer.core.exceptions

import java.util.UUID

class InsufficientBalanceException(
    accountId: UUID
) : RuntimeException("account $accountId does not have enough balance for this operation.")
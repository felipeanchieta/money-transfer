package io.github.felipeanchieta.moneytransfer.account.balance.presenter

import io.github.felipeanchieta.moneytransfer.account.balance.core.BalanceQueryInteractor
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import org.koin.ktor.ext.inject
import java.util.UUID

fun Routing.getAccountBalanceEndpoint() {
    val queryInteractor: BalanceQueryInteractor by inject()
    get("/accounts/{accountId}/balance") {
        val accountId = UUID.fromString(call.parameters["accountId"])
        val balance = queryInteractor.getBalanceForAccount(accountId)
        call.respond(
            status = HttpStatusCode.OK,
            message = AccountBalanceResponse(
                balance = balance
            )
        )
    }
}

private data class AccountBalanceResponse(
    val balance: Double
)
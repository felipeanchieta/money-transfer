package io.github.felipeanchieta.moneytransfer.account.transfer.presenter

import io.github.felipeanchieta.moneytransfer.account.transfer.core.MoneyTransferInteractor
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.post
import org.koin.ktor.ext.inject
import java.util.UUID

fun Routing.transferMoneyEndpoint() {
    val transferInteractor: MoneyTransferInteractor by inject()
    post("/accounts/{accountId}/transfer") {
        val request = call.receive<MoneyTransferRequest>()
        val senderId = UUID.fromString(call.parameters["accountId"])
        val recipientId = UUID.fromString(request.recipientId)

        transferInteractor.transferMoney(request.amount, senderId, recipientId = recipientId)

        call.respond(
            status = HttpStatusCode.Accepted,
            message = MoneyTransferResponse(amount = request.amount)
        )
    }
}

private data class MoneyTransferRequest(
    val recipientId: String,
    val amount: Double
)

private data class MoneyTransferResponse(
    val amount: Double
)
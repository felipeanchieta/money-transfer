package io.github.felipeanchieta.moneytransfer.account.commons.exceptions

import java.util.UUID

class AccountNotFoundException(
    accountId: UUID
) : RuntimeException("account $accountId cannot be found.")
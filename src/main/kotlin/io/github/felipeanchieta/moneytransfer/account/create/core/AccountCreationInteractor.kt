package io.github.felipeanchieta.moneytransfer.account.create.core

import io.github.felipeanchieta.moneytransfer.account.commons.Account
import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import java.util.UUID

class AccountCreationInteractor(private val gateway: AccountsGateway) {
    fun createAccount(initialDeposit: Double, uuid: () -> UUID = { UUID.randomUUID() }): Account {
        return gateway.save(
            Account(
                id = uuid(),
                balance = initialDeposit
            )
        )
    }
}
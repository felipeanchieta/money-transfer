package io.github.felipeanchieta.moneytransfer.account.create.presenter

import io.github.felipeanchieta.moneytransfer.account.create.core.AccountCreationInteractor
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.post
import org.koin.ktor.ext.inject

fun Routing.createAccountEndpoint() {
    val creationInteractor: AccountCreationInteractor by inject()
    post("/accounts") {
        val request = call.receive<NewAccountRequest>()
        val account = creationInteractor.createAccount(initialDeposit = request.initialDeposit)
        call.respond(
            status = HttpStatusCode.Created,
            message = NewAccountResponse(
                accountId = account.id.toString(),
                balance = account.balance
            )
        )
    }
}

private data class NewAccountRequest(
    val initialDeposit: Double
)

private data class NewAccountResponse(
    val accountId: String,
    val balance: Double
)
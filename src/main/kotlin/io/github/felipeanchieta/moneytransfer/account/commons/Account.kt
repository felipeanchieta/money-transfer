package io.github.felipeanchieta.moneytransfer.account.commons

import java.util.UUID

data class Account(
    val id: UUID,
    val balance: Double
)
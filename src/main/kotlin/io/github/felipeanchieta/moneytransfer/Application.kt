package io.github.felipeanchieta.moneytransfer

import io.github.felipeanchieta.moneytransfer.misc.module
import io.ktor.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun main() {
    embeddedServer(Netty, 8080, module = Application::module).start(wait = true)
}
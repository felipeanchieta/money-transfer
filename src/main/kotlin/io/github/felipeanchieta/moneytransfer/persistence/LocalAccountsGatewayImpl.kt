package io.github.felipeanchieta.moneytransfer.persistence

import io.github.felipeanchieta.moneytransfer.account.commons.Account
import io.github.felipeanchieta.moneytransfer.account.commons.AccountsGateway
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap

class LocalAccountsGatewayImpl(
    private val map: ConcurrentHashMap<UUID, Account> = ConcurrentHashMap()
) : AccountsGateway {
    override fun findById(id: UUID): Account? {
        return map[id]
    }

    override fun save(account: Account): Account {
        map[account.id] = account
        return account
    }

    override fun saveAll(accounts: List<Account>): List<Account> {
        map.putAll(accounts.map { account -> account.id to account })
        return accounts
    }
}
package io.github.felipeanchieta.moneytransfer.health

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get

fun Routing.healthEndpoint() {
    get("/health") {
        call.respondText("Healthy :)", ContentType.Text.Plain)
    }
}